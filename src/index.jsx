/**
  * <ReactFormBuilder />
*/

import React from 'react';
import { DndProvider } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import Preview from './preview';
import Toolbar from './toolbar';
import ReactFormGenerator from './form';
import store from './stores/store';

// import withDragDropContext from './html-backend';

const data = [
  {
    "text": "Tabs",
    "id": "1776B860-7848-4137-865F-DC105D8FE814",
    "canPopulateFromApi": true,
    "canHaveAlternateForm": true,
    "dirty": false,
    "element": "Tabs",
    "options": [
      {
        "text": "Ambulance Tab 1",
        "value": "AMBULANCE_TAB_1",
        "key": "tab_option_B51331E5-627B-4C65-9833-E9EE8DE7D203",
        "childForm":[{
          "id": "E5247582-95B4-4215-9B41-DF2EF8652419",
          "element": "Menus",
          "text": "Menus",
          "required": false,
          "canHaveAnswer": true,
          "canHavePageBreakBefore": true,
          "canHaveAlternateForm": true,
          "canHaveDisplayHorizontal": true,
          "canHaveOptionCorrect": true,
          "canHaveOptionValue": true,
          "canPopulateFromApi": true,
          "field_name": "menus_9D4C7426-4A2B-4C1A-8685-CC67C9B1F9A1",
          "label": "Placeholder Label",
          "options": [
            {
              "value": "MENU_1",
              "text": "Menu 1",
              "key": "menu_276276EA-5ACE-4B3E-99B4-284733AF121C"
            },
            {
              "value": "MENU_2",
              "text": "Menu 2",
              "key": "menu_61C2AFAC-3EAC-48AB-8388-030F92BAC895"
            }
          ]
        }]
      },
      {
        "text": "Ambulance Tab 2",
        "value": "AMBULANCE_TAB_2",
        "key": "tab_option_AF17E924-E78A-4BCE-8967-6988FECB6CF2",
        child:'a'
      },
      {
        "text": "Ambulance Tab 3",
        "value": "AMBULANCE_TAB_3",
        "key": "0B68BA8B-5526-4B62-A9E5-2AAD7EB7A98E"
      }
    ],
    "canHaveOptionValue": true,
    "canHaveAnswer": true,
    "label": "Placeholder Label ",
    "canHavePageBreakBefore": true,
    "field_name": "tabs_1745C3A5-CE06-4367-A665-11E559358317",
    "required": false,
    "canHaveDisplayHorizontal": true,
    "canHaveOptionCorrect": true
  },
  {
    "id": "E5247582-95B4-4215-9B41-DF2EF8652418",
    "element": "Menus",
    "text": "Menus",
    "required": false,
    "canHaveAnswer": true,
    "canHavePageBreakBefore": true,
    "canHaveAlternateForm": true,
    "canHaveDisplayHorizontal": true,
    "canHaveOptionCorrect": true,
    "canHaveOptionValue": true,
    "canPopulateFromApi": true,
    "field_name": "menus_9D4C7426-4A2B-4C1A-8685-CC67C9B1F9A0",
    "label": "Placeholder Label",
    "options": [
      {
        "value": "MENU_1",
        "text": "Menu 1",
        "key": "menu_276276EA-5ACE-4B3E-99B4-284733AF121C",
        "childForm":[
          {
            "id": "FE768DB6-7AE7-427A-B1FD-0855D3492883",
            "element": "Dropdown",
            "text": "Dropdown",
            "required": false,
            "canHaveAnswer": true,
            "canHavePageBreakBefore": true,
            "canHaveAlternateForm": true,
            "canHaveDisplayHorizontal": true,
            "canHaveOptionCorrect": true,
            "canHaveOptionValue": true,
            "canPopulateFromApi": true,
            "field_name": "dropdown_B2CDABFA-167B-4583-9B38-283537FB97FC",
            "label": "Placeholder Label",
            "options": [
              {
                "value": "Opt_1",
                "text": "Place holder option 1",
                "key": "dropdown_option_40422E7A-E70E-417E-A18E-0046549E1441"
              },
              {
                "value": "Opt_2",
                "text": "Place holder option 2",
                "key": "dropdown_option_58B971D3-590A-4970-A441-946CBB55D9DA"
              },
              {
                "value": "Opt_3",
                "text": "Place holder option 3",
                "key": "dropdown_option_DADD6084-7184-4223-8BB7-F140C0E5E8C7"
              }
            ],
            "dirty": true
          }
        ]
      },
      {
        "value": "MENU_2",
        "text": "Menu 2",
        "key": "menu_61C2AFAC-3EAC-48AB-8388-030F92BAC895"
      }
    ]
  },
  {
    "id": "FE768DB6-7AE7-427A-B1FD-0855D3492883",
    "element": "Dropdown",
    "text": "Dropdown",
    "required": false,
    "canHaveAnswer": true,
    "canHavePageBreakBefore": true,
    "canHaveAlternateForm": true,
    "canHaveDisplayHorizontal": true,
    "canHaveOptionCorrect": true,
    "canHaveOptionValue": true,
    "canPopulateFromApi": true,
    "field_name": "dropdown_B2CDABFA-167B-4583-9B38-283537FB97FC",
    "label": "Placeholder Label",
    "options": [
      {
        "value": "Opt_1",
        "text": "Place holder option 1",
        "key": "dropdown_option_40422E7A-E70E-417E-A18E-0046549E1441"
      },
      {
        "value": "Opt_2",
        "text": "Place holder option 2",
        "key": "dropdown_option_58B971D3-590A-4970-A441-946CBB55D9DA"
      },
      {
        "value": "Opt_3",
        "text": "Place holder option 3",
        "key": "dropdown_option_DADD6084-7184-4223-8BB7-F140C0E5E8C7"
      }
    ],
    "dirty": true
  },
  {
    "id": "D759F89A-A111-40D2-8042-F4A6B9DAC3F0",
    "element": "RadioButtons",
    "text": "Multiple Choice",
    "required": false,
    "canHaveAnswer": true,
    "canHavePageBreakBefore": true,
    "canHaveAlternateForm": true,
    "canHaveDisplayHorizontal": true,
    "canHaveOptionCorrect": true,
    "canHaveOptionValue": true,
    "canPopulateFromApi": true,
    "field_name": "radiobuttons_A5A08A58-C952-4F20-A362-071E2731FB30",
    "label": "Placeholder Label",
    "options": [
      {
        "value": "place_holder_option_1",
        "text": "Place holder option 1",
        "key": "radiobuttons_option_3AE866F1-8F62-4BEA-BB88-83F7607CDE89",
        "child": "a",
        "childForm":[
          {
            "id": "C54A3D54-24CE-4A1D-937D-C91C4F2242A4",
            "element": "TextInput",
            "text": "Text Input",
            "required": false,
            "canHaveAnswer": true,
            "canHavePageBreakBefore": true,
            "canHaveAlternateForm": true,
            "canHaveDisplayHorizontal": true,
            "canHaveOptionCorrect": true,
            "canHaveOptionValue": true,
            "canPopulateFromApi": true,
            "field_name": "text_input_E89CC7EC-F26D-41A7-BF0F-5C2BD5527875",
            "label": "Placeholder Label"
          }
        ]
      },
      {
        "value": "place_holder_option_2",
        "text": "Place holder option 2",
        "key": "radiobuttons_option_0442D384-4E87-432D-B051-F6BBF55D40DD"
      },
      {
        "value": "place_holder_option_3",
        "text": "Place holder option 3",
        "key": "radiobuttons_option_1B983F3C-C796-4A77-8EC5-7DBA32C6CE3D"
      }
    ]
  },
  {
    "id": "85252221-4F7D-41A4-951C-26D6FE76EEAE",
    "element": "Checkboxes",
    "text": "Checkboxes",
    "required": false,
    "canHaveAnswer": true,
    "canHavePageBreakBefore": true,
    "canHaveAlternateForm": true,
    "canHaveDisplayHorizontal": true,
    "canHaveOptionCorrect": true,
    "canHaveOptionValue": true,
    "canPopulateFromApi": true,
    "field_name": "checkboxes_F300938A-F041-48BA-9947-D93CAC41948D",
    "label": "Placeholder Label",
    "options": [
      {
        "value": "place_holder_option_1",
        "text": "Place holder option 1",
        "key": "checkboxes_option_982EA58F-8911-46AF-8888-1FC6312D8FC9"
      },
      {
        "value": "place_holder_option_2",
        "text": "Place holder option 2",
        "key": "checkboxes_option_6C23CBE5-D642-42EF-A631-D5A650B16096"
      },
      {
        "value": "place_holder_option_3",
        "text": "Place holder option 3",
        "key": "checkboxes_option_007ED697-3648-4EAF-9973-CC0483D150AE"
      }
    ]
  }
]

class ReactFormBuilder extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      editMode: false,
      editElement: null,
    };
    this.editModeOn = this.editModeOn.bind(this);
  }

  editModeOn(data, e) {
    e.preventDefault();
    e.stopPropagation();
    if (this.state.editMode) {
      this.setState({ editMode: !this.state.editMode, editElement: null });
    } else {
      this.setState({ editMode: !this.state.editMode, editElement: data });
    }
  }

  manualEditModeOff() {
    if (this.state.editMode) {
      this.setState({
        editMode: false,
        editElement: null,
      });
    }
  }
  //  componentDidMount() {
  //    setTimeout(()=>{
  //      this.setState({data})
  //    },5000)
  //  }
  render() {
    console.log(this,store, 'this===')
    const toolbarProps = {
      showDescription: this.props.show_description,
    };
    if (this.props.toolbarItems) { toolbarProps.items = this.props.toolbarItems; }
    if (this.props.fragmentRender) {
      return (
        <div>
         {/* <div>
           <p>
             It is easy to implement a sortable interface with React DnD. Just make
             the same component both a drag source and a drop target, and reorder
             the data in the <code>hover</code> handler.
           </p>
           <Container />
         </div> */}
         <div className="react-form-builder clearfix">
           <div>
             <Preview files={this.props.files}
                 manualEditModeOff={this.manualEditModeOff.bind(this)}
                 showCorrectColumn={this.props.showCorrectColumn}
                 parent={this}
                 data={this.props.data}
                 url={this.props.url}
                 saveUrl={this.props.saveUrl}
                 onLoad={this.props.onLoad}
                 onPost={this.props.onPost}
                 editModeOn={this.editModeOn}
                 editMode={this.state.editMode}
                 variables={this.props.variables}
                 editElement={this.state.editElement} 
                 defaultData={this.props.defaultData||[]}
                 formOptions={this.props.formOptions}
                 />
             <Toolbar {...toolbarProps} />
           </div>
         </div>
        </div>
      )
    }
    return (
      <DndProvider backend={this.props.backend || HTML5Backend}>
       <div>
         {/* <div>
           <p>
             It is easy to implement a sortable interface with React DnD. Just make
             the same component both a drag source and a drop target, and reorder
             the data in the <code>hover</code> handler.
           </p>
           <Container />
         </div> */}
         <div className="react-form-builder clearfix">
           <div>
             <Preview files={this.props.files}
                 manualEditModeOff={this.manualEditModeOff.bind(this)}
                 showCorrectColumn={this.props.showCorrectColumn}
                 parent={this}
                 data={this.props.data}
                 url={this.props.url}
                 saveUrl={this.props.saveUrl}
                 onLoad={this.props.onLoad}
                 onPost={this.props.onPost}
                 editModeOn={this.editModeOn}
                 editMode={this.state.editMode}
                 variables={this.props.variables}
                 editElement={this.state.editElement} 
                 defaultData={this.props.defaultData||[]}
                 formOptions={this.props.formOptions}
                 />
             <Toolbar {...toolbarProps} />
           </div>
         </div>
        </div>
      </DndProvider>
    );
  }
}

const FormBuilders = {};
const FBuilder =ReactFormBuilder;
// console.log(FBuilder,withDragDropContext,'FBuilder');
FormBuilders.ReactFormBuilder = FBuilder;
FormBuilders.ReactFormGenerator = ReactFormGenerator;
FormBuilders.ElementStore = store;

export default FormBuilders;

export { ReactFormBuilder, ReactFormGenerator, store as ElementStore };
